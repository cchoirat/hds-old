# Preface

# About the SDSC

# About the authors

# Introduction



# Health Data Science

## A simple `make` example

## Automatic workflow creation

## Automatic knowledge graph creation

# Renku images

## Python base image

## R and RStudio Server

## Creating custom images

## Big data stacks

# Creating R packages with Renku
	
# Working with data

## Retrieving data from a URL

## Uploading data from a local source

## Interfacing with data repositories

# Renku workflows

## Inputs

## Outputs

## Portability

## Execution

# Knowledge Graph

# Conclusion
